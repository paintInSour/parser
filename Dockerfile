FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} Application.jar
ENTRYPOINT ["java","-jar","/Application.jar"]
