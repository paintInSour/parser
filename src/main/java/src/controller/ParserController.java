package src.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import src.service.ParcerService;
import src.site.Site;

@RestController
public class ParserController {

	@Autowired
	private ParcerService service;

	@GetMapping(path = "/getCookDrive")
	public Object getCookDrive() {
		return service.parse(Site.COOKDRIVE);
	}

}
