package src.parser;

import src.site.Site;

public interface Parser {

	public Object parse();
	
	public Site getSite();
}
