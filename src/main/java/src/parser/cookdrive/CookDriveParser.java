package src.parser.cookdrive;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import src.model.cookdrive.category.MenuItem;
import src.model.cookdrive.subcategory.Subcategory;
import src.parser.Parser;
import src.site.Site;
import src.transformer.cookdrive.Transformer;

@Component
@Slf4j
@RequiredArgsConstructor
public class CookDriveParser implements Parser {
    private static final String ITEM_SLECTOR = "li.catalog__list-item article.catalog__item";
    private static final Transformer transformer = new Transformer();
    public static final String cookDriveUrl = "http://cookdrive.com.ua";

    public static Document doc;

    public Site getSite() {
        return Site.COOKDRIVE;
    }

    public static List<MenuItem> getCategory(String categorySelector) throws IOException {
        Elements elements = doc.select(categorySelector);
        List<MenuItem> menuItemList = elements.stream()
            .map(item -> transformer.toMenuItem(item))
            .collect(Collectors.toList());

        return menuItemList;

    }

    public static Elements getSubcategory(String subcategorySelector) {
        return doc.select(subcategorySelector);

    }

    public static void getProduct(List<Subcategory> subcategories, Transformer transformer) throws IOException {
        subcategories.forEach(subcategory -> {
            try {
                doc = Jsoup.connect(subcategory.getUrl())
                    .get();
                Elements products = doc.select(ITEM_SLECTOR);

                subcategory.setProducts(transformer.elementsToProduct(products));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public Object parse() {
        try {
            doc = Jsoup.connect(cookDriveUrl)
                .get();
        } catch (NullPointerException | IOException e2) {
            log.error(e2.getMessage());
        }

        List<MenuItem> list = new ArrayList<MenuItem>();

        try {
            list = getCategory("ul.menu__list li a:has(span)");
        } catch (NullPointerException | IOException e1) {
            log.error(e1.getMessage());
        }

        list.stream()
            .forEach(item ->
            {
                item.setSubcategories(transformer.toSubcategories(getSubcategory("ul.menu__list li a[href = "
                        + item.getUrl() + "] + ul.subnav li a")));

                if (item.getSubcategories()
                    .size() == 0) {
                    item.setSubcategories(Arrays
                        .asList(new Subcategory(item.getCategoryName(), "http://cookdrive.com.ua" + item.getUrl())));
                }

                try {
                    getProduct(item.getSubcategories(), transformer);
                } catch (NullPointerException | IOException e) {
                    log.error(e.getMessage());
                }

            });
        return list;
    }

}
