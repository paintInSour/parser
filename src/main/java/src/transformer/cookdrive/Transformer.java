package src.transformer.cookdrive;

import static src.parser.cookdrive.CookDriveParser.cookDriveUrl;

import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import src.model.cookdrive.category.MenuItem;
import src.model.cookdrive.product.Product;
import src.model.cookdrive.subcategory.Subcategory;


public class Transformer {

    public static String getItemPrice(Element product) {
        return product.getElementsByClass("catalog__item-price-value").text();
    }

    public static String getItemWeight(Element product) {
        return product.getElementsByClass("catalog__item-feature").text();
    }

    public static String getItemOutline(Element product) {
        return product.getElementsByClass("catalog__item-desc").last().child(1).text();
    }

    public static String getItemTitle(Element product) {
        return product.getElementsByClass("catalog__item-title").text();
    }

    public static String getItemImg(Element product) {
        return cookDriveUrl + product.child(0).child(0).child(0).attr("src");
    }

    public static String getOrder(Element product) {
        return cookDriveUrl + product.getElementsByClass("my-add-to-cart").attr("href");
    }

    public static List<Product> elementsToProduct(Elements products) {
        return products
                .stream().map(product -> new Product(getItemTitle(product), getItemOutline(product),
                        getItemWeight(product), getItemPrice(product), getItemImg(product), getOrder(product)))
                .collect(Collectors.toList());
    }
    
    public MenuItem toMenuItem(Element element) {

        Subcategory sub = new Subcategory(element.text(), element.absUrl("href"));
        return MenuItem.builder()
            .categoryName(element.text())
            .url(element.attr("href"))
            .build();

    }

    public List<Subcategory> toSubcategories(Elements subcategories) {
        return subcategories.stream()
            .map(sub -> new Subcategory(sub.text(), sub.absUrl("href")))
            .collect(Collectors.toList());
    }

}
