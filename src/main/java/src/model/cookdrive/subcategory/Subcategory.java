package src.model.cookdrive.subcategory;

import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import src.model.cookdrive.product.Product;

@Getter
@Setter
public class Subcategory {
	private String subcategoryName;
	private String url;

	private List<Product> products;

	public Subcategory(String text, String url) {
		this.subcategoryName = text;
		this.url = url;
	}
}
