package src.model.cookdrive.product;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Product {

	private String title;
	private String description;
	private String weight;
	private String price;
	private String img;
	private String orderUrl;
}
