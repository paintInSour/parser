package src.model.cookdrive.category;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import src.model.cookdrive.subcategory.Subcategory;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MenuItem {
    private String categoryName;
    private String url;
    private List<Subcategory> subcategories;
    
}
