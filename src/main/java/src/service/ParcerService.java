package src.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import src.parser.Parser;
import src.site.Site;

@Service
public class ParcerService {

	@Autowired
	private List<Parser> parsers;

	public Object parse(Site site) {
		return parsers.stream().filter(parser -> parser.getSite() == site).findFirst().get().parse();
	}
}
